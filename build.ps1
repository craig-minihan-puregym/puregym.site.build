# A script to build the PureGym.Site
#
# executes:
# - git clean
# - yarn
# - NuGet and msbuild
# - Jasmine and NUnit tests
# - ExtentReports
#
# requires in the path:
# - git
# - nuget
# - yarn
# - gulp
# - nodejs
#
# NB. The command line can optionally exclude steps as required

#Requires -Version 5.0

param (
    [string] $slnName = "PureGym.AllWebApps.sln",
    [string] $configuration = "Debug",
    [string] $platform = "AnyCPU",
    [Parameter(Mandatory)][string] $repoDir,
    [string] $srcDir = "$(Resolve-Path -ErrorAction Stop $repoDir)/src",
    [string] $packagesDir = "$(Resolve-Path -ErrorAction Stop $srcDir)/packages",
    [string] $testReportsDir = "$(Resolve-Path -ErrorAction Stop $repoDir)/TestReports",
    [int] $testThreads = [Math]::Max(2, ($env:NUMBER_OF_PROCESSORS / 2) + 1),
    [string] $nunitTestDir = "$(Resolve-Path -ErrorAction Stop $srcDir)/TestAssemblies/net472",
    [string] $jasmineTestDir = "$(Resolve-Path -ErrorAction Stop $srcDir)/PureGym.WebUi.Tests",
    [string[]] $nugetSourceUrls = @("https://api.nuget.org/v3/index.json", "http://dev-ne-nuget2-puregym.azurewebsites.net/nuget"),
    [string] $vsVariant = "Professional",
    [switch] $disableUpdatePath,
    [switch] $disableClean,
    [switch] $disableYarn,
    [switch] $disableGulp,
    [switch] $disableBuild,
    [switch] $disableTests,
    [switch] $disableUnitTests,
    [switch] $disableFeatureTests,
    [switch] $disableJasmineTests,
    [switch] $disableTestReports
)

$ErrorActionPreference = "Stop"

function Invoke-Process(
        [Parameter(Mandatory=$true)][string[]] $processArgs,
        [string] $workingDirectory = $pwd) {
    Write-Host "$processArgs -WorkingDirectory ""$workingDirectory""" -ForegroundColor DarkCyan

    [System.Diagnostics.ProcessStartInfo] $pinfo = [System.Diagnostics.ProcessStartInfo]::new($processArgs[0], "$($processArgs[1..$processArgs.Count])")
    $pinfo.UseShellExecute = $false
    $pinfo.WorkingDirectory = $workingDirectory

    [System.Diagnostics.Process] $p = [System.Diagnostics.Process]::new()
    $p.StartInfo = $pinfo
    return &{ if ($p.Start()) { $p } else { $null } }
}

function Update-Path([string] $vsVariant = "Professional") {
    [string[]] $paths = @(
        "${env:ProgramFiles(x86)}\Microsoft SDKs\TypeScript\3.1",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\Common7\IDE\CommonExtensions\Microsoft\TestWindow",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\Common7\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\MSBuild\15.0\bin\Roslyn",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\Team Tools\Performance Tools",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\Shared\Common\VSPerfCollectionTools\",
        "${env:ProgramFiles(x86)}\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\Common7\IDE\CommonExtensions\Microsoft\FSharp\",
        "${env:ProgramFiles(x86)}\Windows Kits\10\bin\10.0.17763.0\x86",
        "${env:ProgramFiles(x86)}\Windows Kits\10\bin\x86",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\MSBuild\15.0\bin",
        "${env:windir}\Microsoft.NET\Framework\v4.0.30319",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\Common7\IDE\",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\${vsVariant}\Common7\Tools\",
        "${env:ProgramFiles}\dotnet\",
        "${env:ProgramFiles(x86)}\NUnit.org\nunit-console",
        "${env:ProgramFiles}\nodejs\",
        "${env:HOMEPATH}\AppData\Roaming\npm",
        "${env:SYSTEMDRIVE}\Python27"
    )

    $paths | Where-Object { !($env:Path.Contains(";$_;") -or $env:Path.EndsWith(";$_")) } | ForEach-Object { $env:Path += ";$_" }
}

function Invoke-Clean([string] $dir) {
    try {
        Push-Location $dir
        git clean -dfxq
        if (!$?) {
            throw "Error: git clean '$dir' failed with code '$LASTEXITCODE'"
        }
    } finally {
        Pop-Location
    }
}

function Invoke-YarnInstall([string] $dir) {
    try {
        Push-Location $dir
        yarn install --pure-lockfile
        if (!$?) {
            throw "Error: yarn install '$dir' failed with code '$LASTEXITCODE'"
        }
    } finally {
        Pop-Location
    } 
}

function Invoke-Gulp([string] $action = "default", [string] $dir) {
    try {
        Push-Location $dir
        gulp $action
        if (!$?) {
            throw "Error: gulp action '$default' failed with code '$LASTEXITCODE'"
        }
    } finally {
        Pop-Location
    }
}

function Invoke-NuGetRestore([string[]] $sources) {
    try {
        Push-Location $srcDir
        nuget restore -Verbosity quiet -Source "$($sources -join ';')" $slnName
        if (!$?) {
            throw "Error: nuget restore '$source' failed with code '$LASTEXITCODE'"
        }
    } finally {
        Pop-Location
    }
}

function Invoke-BuildSln() {
    try {
        Push-Location $srcDir
        msbuild $slnName /p:Configuration=$configuration /p:Platform=$platform /m /nodeReuse:false /verbosity:minimal
        if (!$?) {
            throw "Error: msbuild '$slnName' failed with code '$LASTEXITCODE'"
        }
    } finally {
        Pop-Location
    }
}

function Get-NUnitTestNames([string] $assembly) {
    [string[]] $tests = nunit3-console --explore --noheader $assembly | `
        Select-String -Pattern '^PureGym\.[a-zA-Z0-9\._]*' | `
        ForEach-Object { $_.Matches } | `
        ForEach-Object { $_.Value } | `
        Sort-Object | `
        Get-Unique
    return $tests
}

function Invoke-NUnitTests() {
    try {
        Push-Location $nunitTestDir
        [string[]] $testAssemblies = @()
        $testAssemblies += Get-ChildItem -File -Filter "PureGym.*.Tests.dll" -Include @() | ForEach-Object { $_.FullName }
        $testAssemblies | ForEach-Object {
            [string] $assemblyName = [System.IO.Path]::GetFileName($_)
            [string] $reportDir = "${testReportsDir}/$assemblyName"
            nunit3-console --workers=1 --work=${reportDir} $_
            if (!$?) { throw "Error: one or more unit tests have failed" }
        }
    } finally {
        Pop-Location
    }
}

function Invoke-SpecTests([string] $assembly, [string[]] $tests) {
    try {
        Push-Location $nunitTestDir
        $tempFile = New-TemporaryFile
        $tests | ForEach-Object { "--test=$_" } | Out-File -FilePath $tempFile.FullName -Encoding ascii
        [string] $assemblyName = [System.IO.Path]::GetFileName($assembly)
        [guid] $guid = [guid]::newguid()
        [string] $reportDir = "${testReportsDir}/$assemblyName/$($guid)"
        $p = Invoke-Process @("nunit3-console.exe", "--process=single", "--work=${reportDir}", "@${tempFile}", $assembly)
        return $p
    } finally {
        Pop-Location
    }
}

function Invoke-ParallelSpecTests([string] $assembly, [int] $threads = $env:NUMBER_OF_PROCESSORS, [int] $blockSize = 32) {
    [int] $failedBlocks = 0

    $threads = [Math]::Max(2, $threads)

    [string[]] $tests = Get-NUnitTestNames $assembly

    [object[]] $processes = [object[]]::new($threads)
    for ([int] $i = 0; $i -lt $tests.Count; $i += $blockSize) {
        [int] $processIndex = -1
        while ($processIndex -lt 0) {
            for ([int] $j = 0; ($j -lt $threads); $j++) {
                [object] $process = $processes[$j]
                if ($null -eq $process) {
                    $processIndex = $j
                    break;
                } elseif ($process.HasExited) {
                    if ($process.ExitCode -gt 0) {
                        $failedBlocks++
                    }
                    $processIndex = $j
                    break;
                }
            }

            if ($processIndex -lt 0) {
                Start-Sleep -Seconds 5
            }
        }
        
        $processes[$processIndex] = Invoke-SpecTests -assembly $assembly -tests ($tests | Select-Object -Skip $i -First $blockSize)
    }

    $processes | ForEach-Object {
        if ($_ -ne $null) {
            $_.WaitForExit()
            if ($_.ExitCode -gt 0) {
                $failedBlocks++
            }
        } 
    }

    if ($failedBlocks -gt 0) {
        throw "Error: one or more feature tests have failed"
    }
}

function Invoke-JasmineTests() {
    try {
        Push-Location $jasmineTestDir
        karma start --single-run --reporters nunit
        if (Test-Path "test-results.xml") { 
            [string] $reportPath = "$testReportsDir/Jasmine/"
            (Test-Path $reportPath) -or (New-Item -ItemType dir -Path $reportPath) | Out-Null
            Move-Item "test-results.xml" $reportPath
        }
        if (!$?) { throw "Error: one or more Jasmine UI tests failed" }
    } finally {
        Pop-Location
    }
}

function Invoke-Tests() {
    [object[]] $failedTests = @()

    if (!$disableUnitTests) {
        try { Invoke-NUnitTests } catch { $failedTests += $_ }
    }

    if (!$disableFeatureTests) {
        try { Invoke-ParallelSpecTests -assembly "$nunitTestDir/PureGym.Specs.dll" -threads $testThreads } catch { $failedTests += $_ }
        try { Invoke-ParallelSpecTests -assembly "$nunitTestDir/Puregym.Reseller.Api.Specs.dll" -threads $testThreads } catch { $failedTests += $_ }
        try { Invoke-ParallelSpecTests -assembly "$srcDir/PureGym.Customer.Api.Web.Specs/bin/$configuration/net472/PureGym.Customer.Api.Web.Specs.dll" -threads $testThreads } catch { $failedTests += $_ }
    }

    if (!$disableJasmineTests) {
        try { Invoke-JasmineTests } catch { $failedTests += $_ }
    }

    $failedTests | ForEach-Object { Write-Host $_.Exception }

    if ($failedTests) {
        throw "Error: one or more unit tests failed"
    }
}

function Invoke-TestReports() {
    try {
        nuget install Extent -Version 0.0.3 -Output $packagesDir -Source "$($nugetSourceUrls -join ';')"
        &"$packagesDir\extent.0.0.3\tools\extent.exe" --merge -d $testReportsDir -o $testReportsDir\Extent
    } finally {

    }
}

[DateTime] $startTime = [DateTime]::UtcNow
try {
    $repoDir = Resolve-Path $repoDir

    if (!$disableUpdatePath) {
        Update-Path -vsVariant $vsVariant
    }

    if (!$disableClean) {
        Invoke-Clean -dir $repoDir
    }

    if (!$disableYarn) {
        Invoke-YarnInstall -dir "$srcDir/PureGym.WebUi"
        Invoke-YarnInstall -dir "$srcDir/PureGym.WebUi.Tests"
        Invoke-YarnInstall -dir "$repoDir/node"
        Invoke-YarnInstall -dir "$repoDir/css"
    }

    if (!$disableBuild) {
        if (!$disableGulp) {
            Invoke-Gulp -dir "$srcDir/PureGym.WebUi"
        }

        Invoke-NuGetRestore -sources $nugetSourceUrls
        Invoke-BuildSln
    }

    if (!$disableTests) {
        if (!$disableTestReports) {
            if (!(Test-Path $testReportsDir)) {
                New-Item $testReportsDir -ItemType "dir" | Out-Null
            }
        }

        try {
            Invoke-Tests
        } finally {
            if (!$disableTestReports) {
                Invoke-TestReports
            }
        }
    }
} finally {
    [DateTime] $endTime = [DateTime]::UtcNow
    [TimeSpan] $buildDuration = $endTime - $startTime
    Write-Host "Build duration: $buildDuration"
}
